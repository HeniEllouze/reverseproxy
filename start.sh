#! /bin/bash

docker network create --subnet "172.18.0.0/16" bridge1
docker network create --subnet "172.19.0.0/16" bridge2
docker network create --subnet "172.20.0.0/16" bridge3

cd app1
docker-compose up -d
cd ..
cd app2
docker-compose up -d
cd ..
cd proxy
docker-compose up -d
